package com.example.jpa;

import formation.tp.dao.EquipeDAO;
import formation.tp.entities.Equipe;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

class EquipeDAOTest {
    private static final String NOM_UNITE_DE_PERSISTANCE = "mon-unite-de-persistance";

    private EntityManagerFactory entityManagerFactory;
    private EquipeDAO equipeDAO;

    @BeforeEach
    void setUp() {
        entityManagerFactory = Persistence.createEntityManagerFactory(NOM_UNITE_DE_PERSISTANCE);
        equipeDAO = new EquipeDAO(entityManagerFactory);
    }

    @AfterEach
    void cleanUp() {
        entityManagerFactory.close();
    }

    @Test
    void insertion_d_une_equipe_en_BD(){
        Equipe equipe = new Equipe();
        equipe.setNom("PSG");
        equipe.setPays("France");
        int id = equipeDAO.create(equipe).getId();
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        Equipe equipeFromDB =  entityManager.find(Equipe.class, id);
        Assertions.assertEquals(equipe.getNom(), equipeFromDB.getNom());
        Assertions.assertEquals(equipe.getPays(), equipeFromDB.getPays());
    }

    @Test
    void application_des_changements_sur_une_equipe_existante(){
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        Equipe equipe = new Equipe();
        equipe.setNom("PSG");
        equipe.setPays("France");
        entityManager.persist(equipe);
        entityManager.getTransaction().commit();
        equipe.setPays("Espagne");
        equipeDAO.update(equipe);
        Equipe equipeFromDB =  entityManager.find(Equipe.class, equipe.getId());
        Assertions.assertEquals(equipe.getPays(), "Espagne");
    }
}
