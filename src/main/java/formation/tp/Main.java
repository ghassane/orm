package formation.tp;

import formation.tp.dao.EquipeDAO;
import formation.tp.entities.Equipe;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Main {
    private static final String NOM_UNITE_DE_PERSISTANCE = "mon-unite-de-persistance";

    public static void main(String[] args) {

        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory(NOM_UNITE_DE_PERSISTANCE);
        EntityManager entityManager = entityManagerFactory.createEntityManager();


        entityManager.getTransaction();
        var equipeDAO = new EquipeDAO(entityManagerFactory);

        var equipe = new Equipe();
        equipe.setNom("PSG");
        equipe.setPays("France");

        Equipe equipeFromDB = equipeDAO.create(equipe);
        System.out.println(String.format("Equipe créée avec ID: %s", equipeFromDB.getId()));
    }
}
