package formation.tp.dao;

public final class DAOOperationException extends RuntimeException {

    public DAOOperationException(Exception cause) {
        super(cause);
    }
}
