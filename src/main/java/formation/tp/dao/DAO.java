package formation.tp.dao;

import java.util.List;

public interface DAO<ID, T> {
    T create(T entity);
    T read(ID id);
    List<T> readAll();
    T update(T entity);
    void delete(T entity);
}
